# README #

Docker for backend(django + mysql + phpmyadmin) + rest authencation(jwt) + frontend(reactjs + redux). Sourcecode based on "https://github.com/viewflow/cookbook/tree/master/_articles/redux_jwt_auth"
Thank you "Mikhail Podgurskiy" for this sourcecode.

# Step 1: building backend

- docker-compose run web django-admin.py startproject config .

- cd backend/myproject > nano settings , add code below to DATABASES = { ... }

  'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'django20',
        'USER': 'root',
		'PASSWORD': 'mypass',
        'HOST': 'db',
        'PORT': 3306,
    }
	
- docker-compose up -d (run background) or: docker-compose up (run foreground to view and bug error)

# Step 2: view backend url

- View http://localhost:8000 (django)

- View http://localhost:8082 (phpmyadmin)

# Step 3: configurate more backend

- docker exec -it c_web_django_rest_jwt_reactjs bash ("c_web_django_rest_jwt_reactjs" is container name)

- Inside the bash command, type: "python3.5 manage.py migrate" to create database for django, then type: "python3.5 manage.py createsuperuser" to create admin user, the last type: "exit" to exit command window.

- View http://localhost:8000/admin , login with admin user

- View http://localhost:8082, to see data of database "django20"
 
# Step 4: build frontend

- Navigate to inside frontend folder

- Install NodeJs, Npm, or Yarn

- Use Npm(Yarn) to install "create-react-app" package

- Use "create-react-app" to install the first ReactJs app 

- View http://localhost:3000 to see Reactjs app

# Step 5: update sourcecode from https://github.com/viewflow/cookbook/tree/master/_articles/redux_jwt_auth

- Read carefully the document from "https://github.com/viewflow/cookbook/tree/master/_articles/redux_jwt_auth"

- Copy source code(backend, frontend) from the link above to /backend/... and /frontend...

- Copy js packages below to /frontend/package.json inside "dependencies" properity and run command "yarn install" or "npm install" to install missing packages:

	"bootstrap": "^4.0.0-beta",
    "history": "^4.7.2",
    "jwt-decode": "^2.2.0",
    "react-redux": "^5.0.6",
    "react-router-redux": "^5.0.0-alpha.6",
    "react-scripts": "1.0.12",
    "react-transition-group": "^1.1.2",
    "reactstrap": "^4.8.0",
    "redux": "^3.7.2",
    "redux-api-middleware": "^2.0.0-beta.3",
    "redux-persist": "^5.0.0-beta.7",
    "redux-persist-transform-filter": "0.0.15"
	
- Run "yarn start" or "npm start" to view result on http://localhost:3000 and you can login to reactjs app throught rest api

# Hope userful for you


